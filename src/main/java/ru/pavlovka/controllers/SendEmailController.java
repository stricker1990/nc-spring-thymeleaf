package ru.pavlovka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Properties;

@Controller
public class SendEmailController {

    @Autowired
    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    private JavaMailSenderImpl mailSender;

    @GetMapping("/send_email")
    public String sendEmailForm(Model model, @RequestParam String email){
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setTo(email);
        model.addAttribute("sendingMail", simpleMailMessage);
        return "send_email";
    }

    @PostMapping("/send_email")
    public String sendEmailSubmit(@ModelAttribute SimpleMailMessage sendingMessage){
        sendingMessage.setFrom("stricker-fer@yandex.ru");

        mailSender.send(sendingMessage);
        return "redirect:/";
    }
}
