package ru.pavlovka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pavlovka.model.FindUser;
import ru.pavlovka.services.UserService;
import ru.pavlovka.model.User;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

    private static String UPLOADED__FOLDER = "temp";

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    @GetMapping("/user")
    public String userForm(Model model){
        model.addAttribute("user", new User());
        return "user";
    }

    @PostMapping("/user")
    public String userSubmit(@Valid User user, BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            return "user";
        }
        userService.add(user);
        return "redirect:/";
    }

    @GetMapping("/user/find")
    public String userFindForm(Model model){
        model.addAttribute("findUser", new User());
        return "findUser";
    }

    @PostMapping("/user/find")
    public String userFindSubmit(Model model, @Valid FindUser findUser, BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            return "findUser";
        }
        List<User> foundUser=userService.find(findUser.getFirstName(), findUser.getLastName());
        model.addAttribute("foundUsers", foundUser);
        if(foundUser.isEmpty()){
            return "redirect:/user/notfound";
        }
        return "findUserResult";
    }

    @GetMapping("/user/notfound")
    public String userNotFound(Model model){
        return "notFound";
    }

    @PostMapping("user/upload")
    public String userUpload(@RequestParam("file") MultipartFile file,
                                   Model model) {

        model.addAttribute("uploadMessage", "");

        if (file.isEmpty()) {
            model.addAttribute("uploadMessage", "Выберите файл для загрузки");
            return "user";
        }

        User user=new User();

        try {

            byte[]bytes = file.getBytes();
            Path path = Paths.get(UPLOADED__FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            model.addAttribute("uploadMessage",
                    "Загрузка завершена '" + file.getOriginalFilename() + "'");

            user=userService.loadFromFile(path);

        } catch (IOException e) {
            e.printStackTrace();
            model.addAttribute("uploadMessage",
                    "Ошибка при чтении файла. Выберите корректный JSON файл.");
        }

        model.addAttribute("user", user);

        return "user";
    }

}
