package ru.pavlovka.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class User {
    @NotBlank
    @Pattern(regexp = "\\p{IsAlphabetic}+", message = "Должно сдержать только буквы")
    private String firstName;
    @NotBlank
    @Pattern(regexp = "\\p{IsAlphabetic}+", message = "Должно сдержать только буквы")
    private String middleName;
    @NotBlank
    @Pattern(regexp = "\\p{IsAlphabetic}+", message = "Должно сдержать только буквы")
    private String lastName;

    private byte age;
    private double salary;

    @Email
    @NotBlank
    private String email;

    private String work;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(middleName, user.middleName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(salary, user.salary) &&
                Objects.equals(email, user.email) &&
                Objects.equals(work, user.work);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, age, salary, email, work);
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", email='" + email + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}
