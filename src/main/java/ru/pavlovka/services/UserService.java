package ru.pavlovka.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import ru.pavlovka.model.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Scope("singleton")
public class UserService {
    private static File file=new File("users.json");

    private ObjectMapper objectMapper=new ObjectMapper();

    private List<User> data;

    public UserService() {
        data=new ArrayList<>();
        try {
            this.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void add(User user) throws IOException {
        data.add(user);
        commit();
    }

    public List<User> find(String firstName, String lastName){
        return data.stream()
                .filter(user->user.getFirstName().equals(firstName) && user.getLastName().equals(lastName))
                .collect(Collectors.toList());
    }

    public User loadFromFile(Path path) throws IOException {
        User user=objectMapper.readValue(new File(String.valueOf(path)), User.class);
        return user;
    }

    private void commit() throws IOException {
        objectMapper.writeValue(file, data);
    }

    private void load() throws IOException {
        if(!file.exists()){
            Files.createFile(file.toPath());
        }
        if(file.length()>0){
            data=new ArrayList<>(Arrays.asList(objectMapper.readValue(file, User[].class)));
        }
    }
}
